case class Titulo(titulo: String)

val tituloJson = json"""{ "titulo": "Jasón y los argonautas"}""".noSpaces

val titulo = decode[Titulo](tituloJson)

val titulosJson = json"""[{ "titulo": "Jasón y los argonautas"}]""".noSpaces

val titulos = decode[List[Titulo]](titulosJson)
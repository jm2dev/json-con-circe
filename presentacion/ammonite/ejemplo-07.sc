import $ivy.`io.circe::circe-generic-extras:0.12.0-M1`

import io.circe.generic.extras.semiauto._

case class Titulo(titulo: String) extends AnyVal
case class Paginas(paginas: Int) extends AnyVal
case class Leido(leido: Boolean) extends AnyVal

case class Libro(titulo: Titulo, paginas: Paginas, leido: Leido)

implicit val tituloEncoder = deriveUnwrappedEncoder[Titulo]
implicit val paginasEncoder = deriveUnwrappedEncoder[Paginas]
implicit val leidoEncoder = deriveUnwrappedEncoder[Leido]

val libris =Libro(Titulo("ADT"),Paginas(1234),Leido(true))

val librisJson = libris.asJson.noSpaces
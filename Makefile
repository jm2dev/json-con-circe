help: ## Print this help
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

fichero=presentacion
codigo=circe-examples-sbt

build:	## Run tests and create slides pdf
	cd $(fichero); make pdf; cd ../$(codigo); sbt test

clean:	## Delete output files
	cd $(fichero); make clean; cd ../$(codigo); sbt clean

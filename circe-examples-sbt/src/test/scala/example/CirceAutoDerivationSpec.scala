package example

import org.specs2.Specification
import io.circe.literal._
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._

class CirceAutoDerivationSpec extends Specification {
  def is = s2"""

This is a specification to check automatic derivation for simple JSONs

  given a valid json string then the right case class is decoded  $e1
  given a case class then the expected json string is generated   $e2
"""

  val leidoJson = json"""{ "leido" : true }""".noSpaces
  val leidosJson = json"""[
{ "leido": true }, 
{ "leido": false }, 
{ "leido": false }]""".noSpaces

  case class Leido(leido: Boolean)

  val leido = decode[Leido](leidoJson)
  val leidos = decode[List[Leido]](leidosJson)

  def e1 = leido must be equalTo(Right(Leido(true)))

  def e2 = Leido(true).asJson.noSpaces must be equalTo(leidoJson)
}

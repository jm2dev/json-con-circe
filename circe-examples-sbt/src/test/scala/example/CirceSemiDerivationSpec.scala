package example

import org.specs2.Specification
import io.circe.literal._
import io.circe.parser._
import io.circe.syntax._
import io.circe.generic.extras.semiauto._

case class Titulo(titulo: String) extends AnyVal
case class Paginas(paginas: Int) extends AnyVal
case class Leido(leido: Boolean) extends AnyVal

class CirceSemiDerivationSpec extends Specification {
  def is = s2"""

This is a specification to check JSON from ADTs

  given a valid json string then the right case class is decoded  $e1
  given a case class then the expected json string is generated   $e2
"""

  implicit val tituloEncoder = deriveUnwrappedEncoder[Titulo]
  implicit val paginasEncoder = deriveUnwrappedEncoder[Paginas]
  implicit val leidoEncoder = deriveUnwrappedEncoder[Leido]
  
  implicit val tituloDecoder = deriveUnwrappedDecoder[Titulo]
  implicit val paginasDecoder = deriveUnwrappedDecoder[Paginas]
  implicit val leidoDecoder = deriveUnwrappedDecoder[Leido]

  import io.circe.generic.auto._
  case class Libro(titulo: Titulo, paginas: Paginas, leido: Leido)

  val libris =Libro(Titulo("ADT"),Paginas(1234),Leido(true))

  val librisJson = json"""{"titulo":"ADT","paginas":1234,"leido":true}""".noSpaces

  def e1 = decode[Libro](librisJson) must be equalTo(Right(libris))

  def e2 = libris.asJson.noSpaces must be equalTo(librisJson)
}

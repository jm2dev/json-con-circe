import io.circe.generic.auto._
import io.circe.parser
import io.circe.syntax._

case class Leido(leido: Boolean)

val leidoJson = json"""{ "leido" : true }""".noSpaces

val leido = decode[Leido](leidoJson)

val leidosJson = json"""[
{ "leido": true }, 
{ "leido": false }, 
{ "leido": false }]""".noSpaces

val leidos = decode[List[Leido]](leidosJson)

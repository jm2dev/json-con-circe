case class Libro(titulo: Titulo, paginas: Paginas, leido: Leido)

val libroJson = json"""{ "titulo": "ADT", "paginas": 123, "leido": false }""".noSpaces

val libro = decode[Libro](libroJson)

// falla :(
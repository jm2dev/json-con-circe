import sbt._

object Dependencies {
  lazy val CirceVersion = "0.12.0-M1"

  lazy val circeCore = "io.circe" %% "circe-core" % CirceVersion
  lazy val circeGeneric = "io.circe" %% "circe-generic" % CirceVersion
  lazy val circeGenericExtras = "io.circe" %% "circe-generic-extras" % CirceVersion
  lazy val circeLiteral = "io.circe" %% "circe-literal" % CirceVersion
  lazy val circeParser = "io.circe" %% "circe-parser" % CirceVersion
  lazy val specs2Core = "org.specs2" %% "specs2-core" % "4.3.4"
}

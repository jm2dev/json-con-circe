case class Paginas(paginas: Int)

val paginasJson = json"""{ "paginas": 12 }""".noSpaces

val paginas = decode[Paginas](paginasJson)

val muchasPaginasJson = json"""[
{ "paginas": 12 }, 
{ "paginas": 24 }, 
{ "paginas": 48 }]""".noSpaces

val muchasPaginas = decode[List[Paginas]](muchasPaginasJson)